# AH - Pooya Django Rest API

### Requirements 
- Python3
- Pipenv

### To run code on your local machine:

- clone repository
- change directory into `ie_final_project_backend`
- run `pipenv shell`
- run `pipenv install`
- change directory into `pooya`
- run `python manage.py migrate`
- run `python manage.py runserver`
