from django.contrib import admin

from .models import Student, Course, Master, CourseTake, Field

# Register your models here.
admin.site.register(Student)
admin.site.register(Course)
admin.site.register(Master)
admin.site.register(CourseTake)
admin.site.register(Field)
