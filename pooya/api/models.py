from django.db import models
from django.utils import timezone


class Field(models.Model):
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title


class Student(models.Model):
    student_id = models.CharField(max_length=12, primary_key=True)
    name = models.CharField(max_length=200)
    family = models.CharField(max_length=200)
    field = models.ForeignKey(Field, on_delete=models.CASCADE)
    unit_count = models.IntegerField()
    created_date = models.DateTimeField(default=timezone.now)
    DEGREES = (
        ('B', 'Bachelor'),
        ('M', 'Master'),
        ('P', 'Doctorate'),
    )
    degree = models.CharField(max_length=1, choices=DEGREES)

    def __str__(self):
        return self.name


class Master(models.Model):
    personal_id = models.CharField(max_length=12, primary_key=True)
    name = models.CharField(max_length=200)
    family = models.CharField(max_length=200)
    field = models.ForeignKey(Field, on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)
    DEGREES = (
        ('B', 'Bachelor'),
        ('M', 'Master'),
        ('P', 'Doctorate'),
    )
    degree = models.CharField(max_length=1, choices=DEGREES)

    def __str__(self):
        return self.name


class Course(models.Model):
    course_id = models.CharField(max_length=12, primary_key=True)
    name = models.CharField(max_length=200)
    unit_count = models.IntegerField()
    course_master = models.ForeignKey(Master, on_delete=models.CASCADE, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    students = models.ManyToManyField(Student, through='CourseTake')
    field = models.ForeignKey(Field, on_delete=models.CASCADE)
    semester = models.IntegerField()

    # //روزو ساعت شمسی\ نمره مثبت

    def __str__(self):
        return self.name


class CourseTake(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    date_taken = models.DateTimeField(default=timezone.now)
    additional_description = models.CharField(max_length=200, null=True, )
