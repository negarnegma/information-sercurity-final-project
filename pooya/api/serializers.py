from rest_framework import serializers

from .models import Course, Student, Master, CourseTake, Field


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['student_id', 'name', 'family', 'field', 'unit_count', 'created_date', 'degree']


class MasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Master
        fields = ['personal_id', 'name', 'family', 'field', 'created_date', 'degree']


class FieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = Field
        fields = ['title']


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ['course_id', 'name', 'unit_count', 'course_master', 'created_date', 'students', 'field','semester']


class CourseTakeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseTake
        fields = ['course', 'student', 'date_taken', 'additional_description']
