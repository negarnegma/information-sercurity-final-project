# Create your tests here.

from pooya.api.models import Field, Master, Course, Student

if __name__ == '__main__':
    Field.objects.create(
        title="مهندسی کامپیوتر"
    )
    Field.objects.create(
        title="مهندسی مکانیک"
    )
    Field.objects.create(
        title="مهندسی برق"
    )

    Master.objects.create(
        personal_id="32779269",
        name="مصطفی",
        family="نوری بایگی",
        field=Field.objects.get(title='مهندسی کامپیوتر'),
        degree='Doctorate'
    )

    Course.objects.create(
        course_id="21300546",
        name="طراحی الگوریتم",
        unit_count=3,
        course_master=Master.objects.get(personal_id="32779269"),
        field=Field.objects.get(title='مهندسی کامپیوتر'),
        # students=[],
        semester=2
    )
    Course.objects.create(
        course_id="21400546",
        name="مبانی بینایی ماشین",
        unit_count=3,
        course_master=Master.objects.get(personal_id="32779269"),
        field=Field.objects.get(title='مهندسی کامپیوتر'),
        # students=[],
        semester=2
    )

    Student.objects.create(
        student_id="9512762594",
        name="نگار",
        family="ضمیری",
        field=Field.objects.get(title='مهندسی کامپیوتر'),
        unit_count=0,
        degree="Bachelor"
    )

    s1 = Student.objects.get(student_id="9512762594")
    c1 = Course.objects.get(course_id="21300546")
    c1.students.add(s1)
    s1.unit_count = s1.unit_count + c1.unit_count
    s1.save()

    s1 = Student.objects.get(student_id="9512762594")
    c1 = Course.objects.get(course_id="21300546")
    if c1.students.filter(student_id="9512762594").exists():
        c1.students.remove(s1)
        s1.unit_count = s1.unit_count - c1.unit_count
    else:
        raise Exception("دانشجوی فعلی این درس را انتخاب نکرده است!")
    s1.save()
