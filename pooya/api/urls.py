from django.urls import path

from . import views

urlpatterns = [
    path('welcome', views.welcome),
    path('getcourse', views.get_courses),
    path('add-course', views.add_course),
    path('final-taed', views.final_taeed),
    path('course/selected', views.get_selected_courses),
    path('course/unselect/<int:course_id>', views.unselect_course),
    path('student/register', views.register_student),
    path('student', views.get_students),
    path('student/<int:student_id>', views.update_student),
    path('master', views.get_masters)
]
