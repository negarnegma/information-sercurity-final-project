import json

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from .models import Master, Course, Field, Student
from .serializers import CourseSerializer, StudentSerializer, MasterSerializer


# Create your views here.
@api_view(["GET"])
@permission_classes([IsAuthenticated])
def welcome(request):
    content = {'message': 'Welcome qqqqq App!'}
    return JsonResponse(content, status=200)


@api_view(["GET"])
@csrf_exempt
@permission_classes([AllowAny])
def get_courses(request):
    payload = request.query_params
    courses = Course.objects
    if request.query_params.get("semester", None) is not None:
        courses = courses.filter(semester=payload["semester"])
    if request.query_params.get("field", None) is not None:
        courses = courses.filter(field=payload["field"])
    serializer = CourseSerializer(courses, many=True)
    return JsonResponse({'courses': serializer.data}, safe=False, status=status.HTTP_200_OK)


@api_view(["POST"])
@csrf_exempt
@permission_classes([AllowAny])
def add_course(request):
    payload = json.loads(request.body)
    course = Course.objects.create(
        course_id=payload["course_id"],
        name=payload['name'],
        unit_count=payload['unit_count'],
        course_master=None if payload['course_master_id'] == 0 else Master.objects.get(payload['course_master_id']),
        # students=payload['students'],
        field=Field.objects.get(title=payload['field']),
        semester=payload['semester']
    )
    serializer = CourseSerializer(course)
    return JsonResponse({'courses': serializer.data}, safe=False, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@csrf_exempt
@permission_classes([IsAuthenticated])
def final_taeed(request):
    student_id = request.user.username
    print('************************')
    print(student_id)
    s1 = Student.objects.get(student_id=student_id)
    payload = json.loads(request.body)
    for i in payload:
        c1 = Course.objects.get(course_id=i)
        if c1.students.filter(student_id=student_id).exists():
            raise Exception("درس %s قبلا انتخاب شده است!" % i)
        else:
            if s1.unit_count + c1.unit_count >6:
                raise Exception("تعداد واحد ها بیشتر از 6 واحد شد!")
            c1.students.add(s1)
            s1.unit_count = s1.unit_count + c1.unit_count
            s1.save()

    return JsonResponse({'message': 'done:)'}, safe=False, status=status.HTTP_201_CREATED)


@api_view(["GET"])
@csrf_exempt
@permission_classes([IsAuthenticated])
def get_selected_courses(request):
    student_id = request.user.username
    courses = Course.objects.all().filter(students__student_id=student_id)
    serializer = CourseSerializer(courses, many=True)
    return JsonResponse({'courses': serializer.data}, safe=False, status=status.HTTP_200_OK)


@api_view(["DELETE"])
@csrf_exempt
@permission_classes([IsAuthenticated])
def unselect_course(request, course_id):
    student_id = request.user.username
    try:
        s1 = Student.objects.get(student_id=student_id)
        c1 = Course.objects.get(course_id=course_id)
        if c1.students.filter(student_id=student_id).exists():
            c1.students.remove(s1)
            s1.unit_count = s1.unit_count - c1.unit_count
        else:
            raise Exception("دانشجوی فعلی درس %s را انتخاب نکرده است!" % course_id)
            # todo exception class ha ro khas kon
        s1.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        return JsonResponse({'error': 'Something went wrong: %s' % str(e)}, safe=False,
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["POST"])
@csrf_exempt
@permission_classes([AllowAny])
def register_student(request):
    payload = json.loads(request.body)
    User.objects.create_user(payload["student_id"], payload["email"], payload["password"])
    Student.objects.create(
        student_id=payload["student_id"],
        name=payload["name"],
        family=payload["family"],
        field=Field.objects.get(id=payload["field_id"]),  # or field=Field.objects.get(title='مهندسی کامپیوتر'),
        unit_count=0,
        degree=payload["degree"]
    )
    return JsonResponse({'message': 'done:)'}, safe=False, status=status.HTTP_201_CREATED)


@api_view(["GET"])
@csrf_exempt
@permission_classes([AllowAny])
def get_students(request):
    payload = request.query_params
    students = Student.objects
    if request.query_params.get("field", None) is not None:
        students = students.filter(field=payload["field"])
    serializer = StudentSerializer(students, many=True)
    return JsonResponse({'students': serializer.data}, safe=False, status=status.HTTP_200_OK)


@api_view(["GET"])
@csrf_exempt
@permission_classes([AllowAny])
def get_masters(request):
    payload = request.query_params
    masters = Master.objects
    if request.query_params.get("field", None) is not None:
        masters = masters.filter(field=payload["field"])
    serializer = MasterSerializer(masters, many=True)
    return JsonResponse({'masters': serializer.data}, safe=False, status=status.HTTP_200_OK)


@api_view(["PUT"])
@csrf_exempt
@permission_classes([AllowAny])
def update_student(request, student_id):
    payload = json.loads(request.body)
    try:
        student_item = Student.objects.filter(student_id=student_id)
        # returns 1 or 0
        print(student_item)
        # if payload["field"]!=student_item.field:
        #     raise Exception('access dennied!')
        # student_item.update(**payload)

        student_item.save(update_fields=['name','family'])
        student = Student.objects.get(student_id=student_id)
        serializer = StudentSerializer(student)
        return JsonResponse({'student': serializer.data}, safe=False, status=status.HTTP_200_OK)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        return JsonResponse({'error': 'Something terrible went wrong: %s'%str(e)}, safe=False,
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
